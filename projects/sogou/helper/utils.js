exports.getLogger = (function () {
    var log4js = require('log4js');
    log4js.configure({
        appenders: [
            { type: 'console' },
            { type: 'file', filename: 'logs/cheese.log', category: 'cheese' }
        ]
    });
    logger = log4js.getLogger('cheese');
    logger.setLevel('DEBUG');
    return function(){
        return logger;
    }
})();