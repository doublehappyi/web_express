var express = require('express');
var router = express.Router();
var config = require('../config');
var helper = require('../helper');
var logger = helper.utils.getLogger();
var mysql = require('mysql');

//微信搜索页
router.get('/', function (req, res, next) {
    res.render('weixin/index.html');
});
router.get('/list', function (req, res, next) {
    res.render('weixin/list.html');
});

//微信文章搜索接口
router.get('/search', function (req, res, next) {
    logger.debug('/search start');
    var pool = req.pool,
        query = req.query,
        maxPageSize = 50;

    logger.info(query);
    var keyword = decodeURIComponent(query.keyword) || ".",
        keywordType = parseInt(query.keywordType),
        keywordType = [1, 2].indexOf(keywordType) < 0 ? 1:keywordType,
        pageSize = !query.pageSize || (query.pageSize && parseInt(query.pageSize) > maxPageSize) ? 10 : parseInt(query.pageSize),
        pageNum = parseInt(query.pageNum) || 1;

    var sql_str;
    if (keywordType === 1) {
        sql_str = getArticleSql(query, keyword, pageSize, pageNum);
        logger.debug('article_sql: '+sql_str);
    } else {
        sql_str = getWeixinSql(keyword, pageSize, pageNum);
        logger.debug('weixin_sql: '+sql_str);
    }

    logger.debug(sql_str);
    pool.getConnection(function (err, conn) {
        if (err) throw err;
        conn.query(sql_str, function (err, results, fields) {
            if (err) throw err;
            res.jsonp({
                retCode: 0, msg: '', data: {totalNum: results[0][0].totalNum, list: results[1]}
            });
        });
        conn.release();
    });
});

router.get('/articles', function (req, res, next) {
    var pool = req.pool,
        query = req.query;

    var sql_str = getTargetArticlesSql(query);
    logger.debug(sql_str)
    pool.getConnection(function (err, conn) {
        if (err) throw err;
        conn.query(sql_str, function (err, results, fields) {
            if (err) throw err;
            var articleListData = results[0],
                totalNumListData = results[1],
                wxListData = results[2];
            var data = wxListData[0];
            data.totalNum = totalNumListData[0].totalNum;
            data.list = articleListData;
            res.jsonp({retCode: 0, msg: '', data: data});
        });
        conn.release();
    });
});

function getTargetArticlesSql(query) {
    //三个数据库的搜索查询语句：
    // 1，文章列表查詢
    // 2，文章总数查询
    // 3，微信号详细信息查询
    logger.debug('getTargetArticlesSql start');
    var maxPageSize = 50;
    var sortFieldMap = {
        'date': 'article_publish_time',
        'read': 'article_read_num',
        'like': 'article_like_num'
    };
    var wx_id = parseInt(query.wxId),
        sortType = ['desc', 'asc'].indexOf(query.sortType) < 0 ? 'desc' : query.sortType,
        sortType = ['desc', 'asc'].indexOf(query.sortType) < 0 ? 'desc' : query.sortType,
        sortField = ['date', 'read', 'like'].indexOf(query.sortField) < 0 ? 'date' : query.sortField,
        sortField = sortFieldMap[sortField],
        pageSize = !query.pageSize || (query.pageSize && parseInt(query.pageSize) > maxPageSize) ? 10 : parseInt(query.pageSize),
        pageNum = parseInt(query.pageNum) || 1;
    logger.debug('pageNum: '+pageNum);
    //, wx_intro, wx_qrcode
    var filter_str = 'where ref_wx_id=? order by article.?? ' + sortType+' ',
        limit_str = 'limit ?, ?';

    var select_str = 'select article_id, article_title, article_url, article_read_num, article_like_num, article_publish_time from article ',
        page_str = select_str + filter_str + limit_str,
        page_str = mysql.format(page_str, [wx_id, sortField, pageSize * (pageNum - 1), pageSize]);

    var select_str = 'select count(article_id) as totalNum from article ',
        count_str = select_str + filter_str,
        count_str = mysql.format(count_str, [wx_id, sortField]);

    var select_str = 'select wx_id, wx_name, wx_account from weixin ',
        filter_str = 'where wx_id=? ',
        wx_str = select_str + filter_str,
        wx_str = mysql.format(wx_str, [wx_id]);
    logger.debug('sql_str: \n'+count_str + ';' + page_str + ';' + wx_str);
    return page_str + ';' + count_str + ';' + wx_str;
}

function getWeixinSql(keyword, pageSize, pageNum) {
    //, wx_intro, wx_qrcode
    var filter_str = 'where wx_name regexp ? ',
        limit_str = 'limit ?, ?'
    var page_str = 'select wx_id, wx_name, wx_account from weixin ' + filter_str + limit_str,
        page_str = mysql.format(page_str, [keyword, pageSize * (pageNum - 1), pageSize]);
    var count_str = 'select count(wx_id) as totalNum from weixin ' + filter_str,
        count_str = mysql.format(count_str, [keyword]);
    return count_str + ';' + page_str;
}

function getArticleSql(query, keyword, pageSize, pageNum) {
    logger.debug('keywordType === 1');
    var sortFieldMap = {
        'date': 'article_publish_time',
        'read': 'article_read_num',
        'like': 'article_like_num'
    };
    var maxPeriod = 1000;
    var period = [0, 3, 7, 15, 30].indexOf(parseInt(query.period)) <= 0 ? maxPeriod : parseInt(query.period),
        sortType = ['desc', 'asc'].indexOf(query.sortType) < 0 ? 'desc' : query.sortType,
        sortField = ['date', 'read', 'like'].indexOf(query.sortField) < 0 ? 'date' : query.sortField,
        sortField = sortFieldMap[sortField];
    var endPublishTime = parseInt(new Date().getTime() / 1000),
        startPublishTime = endPublishTime - period * 24 * 60 * 60;

    logger.debug('article search query: ' + 'period: ' + period + 'sortType: ' + sortType + 'sortField: ' + sortField);
    var filer_str = 'where article.article_title regexp ? ' +
            'and article.article_publish_time>? and article.article_publish_time<? ' +
            'and article.ref_wx_id=weixin.wx_id ' +
            'order by article.?? ' + sortType+' ',
        limit_str = 'limit ?, ?',
        select_str = 'select ' +
            'article.article_id, article.article_title, article.article_url, article.article_read_num, ' +
            'article.article_like_num, article.article_publish_time, ' +
            'weixin.wx_id, weixin.wx_name, weixin.wx_account ' +
            'from article, weixin ' + filer_str;
    var page_str = select_str + limit_str,
        page_str = mysql.format(page_str, [keyword, startPublishTime, endPublishTime, sortField, pageSize * (pageNum - 1), pageSize]);

    var count_str = 'select count(article.article_id) as totalNum from article, weixin ' + filer_str,
        count_str = mysql.format(count_str, [keyword, startPublishTime, endPublishTime, sortField]);
    logger.debug('count_str: ' + count_str);
    return count_str + ';' + page_str;
}

module.exports = router;
