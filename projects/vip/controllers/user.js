var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var helper = require('../helper');
var logger = helper.utils.getLogger();

router.get('/', function(req, res, next) {
   res.render('home.html'); 
});

/* GET users listing. */
router.get('/register', function (req, res, next) {
    res.cookie('test', 'ok');
    res.render('register');
});

router.post('/register', function (req, res, next) {
    // res.json(req.body);
    var pool = req.pool;
    var phone = req.body.phone,
        password = req.body.password;
    // res.json(req.body);   
    if (!phone || !password){
        res.json({retCode: 1, msg:'输入数据不合法'});
    }
    
    if (phone.length != 11 || !parseInt(phone)){
        res.json({retCode:1, msg:'手机号码不合法，手机号码为11位整数'});
    }

    if (!password || password.length < 6 || password.length > 20){
        res.json({retCode:1, msg:'密码不合法，密码长度大于6,小于20'});
    }

    logger.debug('phone:' + phone + ' password: ' + password);
    pool.getConnection(function (err, conn) {
        logger.debug('get connection');
        if (err) throw err;
        var query_str = 'select count(id) as count from user where phone=?',
            query_str = mysql.format(query_str, [phone]);

        conn.query(query_str, function (err, rows, fields) {
            logger.debug('query 1');
            if (err) throw err;
            logger.debug('rows.length: ' + rows.length);
            if (!rows[0].count) {
                var insert_str = 'insert into user values (null, null, ?, ?, null, null, ?)',
                    insert_str = mysql.format(insert_str, [password, phone, parseInt(new Date().getTime() / 1000)]);

                logger.debug('insert_str: ' + insert_str);
                conn.query(insert_str, function (err, result) {
                    logger.debug('query 2');
                    if (err) throw err;
                    logger.debug('insertId: ' + result.insertId);
                    res.json({retCode: 0, msg: 'ok'});
                    conn.release();
                });
            } else {
                res.json({retCode: 1, msg: '手机号码' + phone + '已注册'});
                conn.release();
            }
        });
    });
});

router.get('/login', function (req, res, next) {
    res.render('login.html');
});

router.post('/login', function (req, res, next) {
    var pool = req.pool;
    var phone = req.body.phone,
        password = req.body.password;

    if (phone.length != 11 || !parseInt(phone)){
        res.json({retCode:1, msg:'手机号码不合法，手机号码为11位整数'});
    }

    if (!password || password.length < 6 || password.length > 20){
        res.json({retCode:1, msg:'密码不合法，密码长度大于6,小于20'});
    }

    logger.debug('phone:' + phone + ' password: ' + password);
    pool.getConnection(function (err, conn) {
        logger.debug('get connection');
        if (err) throw err;
        var query_str = 'select id from user where phone=? and password=?',
            query_str = mysql.format(query_str, [phone, password]);

        conn.query(query_str, function (err, rows, fields) {
            logger.debug('query 1');
            if (err) throw err;
            logger.debug('rows.length: ' + rows.length);
            if (rows.length && rows[0].id) {
                res.json({retCode: 0, msg: '登录成功, id为：'+rows[0].id});
            } else {
                res.json({retCode: 1, msg: '登录失败，用户名或密码错误'});
                conn.release();
            }
        });
    });
});


module.exports = router;
