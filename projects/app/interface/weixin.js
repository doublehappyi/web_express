//以下接口如果获取失败，那么retCode=1

//文章搜索获取接口：
//GET /weixin/search?keyword=a&keywordType=1&period=3/7/15/30&pageSize=10&pageNum=1&sortType=desc&sortField=date/read/like
//GET /weixin/search?keyword=keyword&keywordType=1&period=3/7/15/30&pageSize=10&pageNum=1&sortType=desc&sortField=date/read/like
d = {
    'retCode': 0,// 0/1
    'msg': 'OK',
    'data': {
        'totalNum': 100,
        'list': [{
            'article_id':'',
            'article_title': '',
            'article_url': '',
            'article_read_num': 0,
            'article_like_num': 0,
            'article_publish_time':'2015-07-01',
            'wx_id':'',
            'wx_name':'',
            'wx_account':''
        },]
    }
}

//微信号搜索获取接口：
//GET /weixin/search?keyword=keyword&keywordType=2&pageSize=10&pageNum=1
d = {
    'retCode': 0,
    'msg': 'ok',
    'data': {
        'totalNum': 100,
        'list': [{
            'wx_id':'',
            'wx_name':'',
            'wx_account':'',
            'wx_intro':'',
            'wx_qrcode':''
        },]
    }
}


//指定微信号下的所有文章
//GET /weixin/articles?wxId=wx_id&pageSize=10&pageNum=1
d = {
    'retCode': 0,
    'msg': 'ok',
    'data': {
        'wx_id':'',
        'wx_name':'',
        'wx_account':'',
        'wx_biz':'',
        'wx_intro':'',
        'wx_qrcode':'',
        'totalNum':100,
        'lists':[{
            'article_id':'',
            'article_title': '',
            'article_url': '',
            'article_read_num': 0,
            'article_like_num': 0,
            'article_publish_time':'2015-07-01'
        },]
    }
}