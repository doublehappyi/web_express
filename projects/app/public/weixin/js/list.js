// 依赖gb.js
$(function () {
    // 获取当前用户信息,只有用户登陆并且已经是cps用户才请求列表
    //common.getCpsUserInfo(function (cbData) {

        var $m = {
            //分类
            cat : $('#cat'),
            catTpl : $('#catTpl').html(),
            //时间
            datepicker1 : $("#J_datepicker1"),
            datepicker2 : $("#J_datepicker2"),

            //商品库
            hotGoods : $("#J_goods"),
            hotGoodsTpl : $("#goodsTpl").html(),
            hotGoodsItemTpl : $("#goodListTpl").html(),
            goodsBox : $("#J_goodsBox"),

            //名品库
            hotShops : $("#hotShops"),
            goodsMain : $('#J_goodsMain'),
            numListTpl : $('#numListTpl').html(),
            //搜索
            searchForm : $('#J_searchForm'),

            //筛选
            filterCategory : $('#J_filterCategory'),
            lastCategory : $(), //存储最后选择的分类
            filterItems : $('#J_filterItems'),
            //排序
            filterCondition : $('#J_filterCondition'),
            filter : $('#J_filter'),
                 //数据
            pageNum : 1,
            pagesize : 10,   //每页加载数量
            curpage : 1,
            countpage : 1,
            //分页
            page : $('#J_page'),
        };


        var $c = {

            //初始化
            init : function () {
                var self = this;

                $(document).lazyload();
                $m.lastContentList = $('#J_goodsMain');
                self.render($m.hotGoods);
                self.bindEvent();

            },

        //加载公众号
        getAccountNum : function () {
            var url = window.location.search;
            var self = this;
            //alert(window.location.search);
            var wxId = self.getHrefField('wxId',location.href);
            //alert(loc);
            
             $.ajax({
                    url: "/weixin/articles",
                    type: "POST",
                    dataType:"jsonp",
                    crossDomain:true,
                    data : {
                        wxId:wxId,
                        pageNum: self.getHrefField('pageNum',location.href)||1, //支持分页，0为第一页
                        pageSize: 10, //每页记录数
                        },

                        success: function (redata, status, xhr) {
                            if (redata.retCode == 0) {
                                var tmplData = [];
                                var tmplDataAccount = [];
                                var list = redata.data.list;
                                var wx_id=redata.data.wx_id,
                                    wx_name=redata.data.wx_name,
                                    wx_account=redata.data.wx_account,
                                    wx_intro =redata.data.wx_intro,
                                    wx_qrcode=redata.data.wx_qrcode;
                                    wx_total_num= redata.data.totalNum;
                                var accountlist = { wx_id: wx_id, wx_name: wx_name, wx_account:wx_account,wx_intro:wx_intro,wx_qrcode:wx_qrcode};
                                for (var i = 0; i < list.length; i++){
                                var time = parseInt(list[i]['article_publish_time']);
                                var d = new Date(time*1000);
                                var article_publish_time = d.getFullYear()+'-'+d.getMonth()+'-'+d.getDay()+' '+d.getHours()+':'+d.getMinutes();
                                list[i]['article_publish_time'] = article_publish_time;
                            }
                        }
                         tmplData = { itemlist:list };
                         var renderlist = Mustache.render($m.hotGoodsTpl, tmplData);
                         $m.hotGoods.html(renderlist);

                         $m.countpage=wx_total_num;
                         self.setPage($m.page[0],$m.countpage,$m.curpage);

                         tmplDataAccount ={ itemlist:accountlist};
                         var render =Mustache.render($m.numListTpl,tmplDataAccount);
                         $m.hotShops.html(render);
                         
                         $(document).lazyload('load');
                           //列表排序
                            var orderType = self.getHrefField('sortType',location.href)||"desc";//desc/asc
                            var fieldOrderType =self.getHrefField('sortField',location.href)||"date";//date/read/like
                            var url = location.href;

                            switch (fieldOrderType) {
                                case "date":
                                if(orderType=="asc"){
                                    $m.hotGoods.find('p').removeClass('on');
                                    $m.hotGoods.find('#J_filterDate').addClass('on');
                                    $m.hotGoods.find('#J_filterDate').children("i").toggleClass("dm_mod_filter_arrow_down");
                                }
                                break;
                                case "read":
                                if(orderType=="asc"){
                                    $m.hotGoods.find('p').removeClass('on');
                                    $('#J_filterRead').addClass('on');
                                    $('#J_filterRead').children("i").toggleClass("dm_mod_filter_arrow_down");
                                }
                                break;
                                case "like":
                                if(orderType=="asc"){
                                    $m.hotGoods.find('p').removeClass('on');
                                    $('#J_filterGoods').addClass('on');
                                    $('#J_filterGoods').children("i").toggleClass("dm_mod_filter_arrow_down");
                                }
                                break;
                                default:
                                break;
                                }
                        }

                    });

        },
        //取出url参数
        getHrefField :function (field, href) {
            var ret = "";
            var href = href || location.href;
            var fieldStr, fieldArr, index;
            if (href.indexOf("?") !== -1) {
                fieldStr = href.substring(href.indexOf("?") + 1);
                fieldArr = fieldStr.split("&");
                for (var i = 0, len = fieldArr.length; i < len; i++) {
                    index = fieldArr[i].indexOf(field + "=");
                    if (index !== -1) {
                        ret = fieldArr[i].substring(index + (field + "=").length);
                        break;
                    }
                }

                return decodeURIComponent(ret);
            } else {
                return decodeURIComponent(ret);
            }
        },
        changeURLPar :function (url, ref, value) {
                var str = "";
                if (url.indexOf('?') != -1)
                    str = url.substr(url.indexOf('?') + 1);
                else
                    return url + "?" + ref + "=" + value;
                var returnurl = "";
                var setparam = "";
                var arr;
                var modify = "0";

                if (str.indexOf('&') != -1) {
                    arr = str.split('&');
                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i].split('=')[0] == ref) {
                            setparam = value;
                            modify = "1";
                        }
                        else {
                            setparam = arr[i].split('=')[1];
                        }
                        returnurl = returnurl + arr[i].split('=')[0] + "=" + setparam + "&";
                    }
                    returnurl = returnurl.substr(0, returnurl.length - 1);
                    if (modify == "0")
                        if (returnurl == str)
                            returnurl = returnurl + "&" + ref + "=" + value;
                }
                else {
                    if (str.indexOf('=') != -1) {
                        arr = str.split('=');
                        if (arr[0] == ref) {
                            setparam = value;
                            modify = "1";
                        }
                        else {
                            setparam = arr[1];
                        }
                        returnurl = arr[0] + "=" + setparam;
                        if (modify == "0")
                            if (returnurl == str)
                                returnurl = returnurl + "&" + ref + "=" + value;
                    }
                    else
                        returnurl = ref + "=" + value;
                }
                return url.substr(0, url.indexOf('?')) + "?" + returnurl;
        },
        setPage :function(con, count, pageindex) {
            var con = con;
            var count = count;
            var pageindex = pageindex;
            var a = [];
              //总页数少于10 全部显示,大于10 显示前3 后3 中间3 其余....
              if (pageindex == 1) {
                a[a.length] = "<a href=\"#\" class=\"prev unclick\">prev</a>";
              } else {
                a[a.length] = "<a href=\"#\" class=\"prev\">prev</a>";
              }
              function setPageList() {
                if (pageindex == i) {
                  a[a.length] = "<a href=\"#\" class=\"on\">" + i + "</a>";
                } else {
                  a[a.length] = "<a href=\"#\">" + i + "</a>";
                }
              }
              //总页数小于10
              if (count <= 10) {
                for (var i = 1; i <= count; i++) {
                  setPageList();
                }
              }
              //总页数大于10页
              else {
                if (pageindex <= 4) {
                  for (var i = 1; i <= 5; i++) {
                    setPageList();
                  }
                  a[a.length] = "...<a href=\"#\">" + count + "</a>";
                } else if (pageindex >= count - 3) {
                  a[a.length] = "<a href=\"#\">1</a>...";
                  for (var i = count - 4; i <= count; i++) {
                    setPageList();
                  }
                }
                else { //当前页在中间部分
                  a[a.length] = "<a href=\"#\">1</a>...";
                  // alert(pageindex-2);
                  // alert(pageindex+2);
                  for (var i = (pageindex-2); i <= (parseInt(pageindex)+2); i++) {
                    setPageList();
                  }
                  a[a.length] = "...<a href=\"#\">" + count + "</a>";
                }
              }
              if (pageindex == count) {
                a[a.length] = "<a href=\"#\" class=\"next unclick\">next</a>";
              } else {
                a[a.length] = "<a href=\"#\" class=\"next\">next</a>";
              }
              con.innerHTML = a.join("");
            },
    

        //绑定事件
        bindEvent : function () {

            var self = this;

        //列表排序
            $m.hotGoods.on('click', '#J_filterCondition p', function (e) {
                e.preventDefault();
                e.stopPropagation();
                var curId = this.id;
                var sort_Field,
                    sort_Type = "";
                switch (curId) {
                    case "J_filterDate":
                        sort_Field="date";
                        break;
                    case "J_filterRead":
                        sort_Field="read";
                        break;
                    case "J_filterGoods": //
                        sort_Field="like";
                        break;
                    default:
                        break;
                }
                var locationUrl=location.href.indexOf("?");
                var url="";
                var URL="";
                if(locationUrl>0){
                    if(self.getHrefField('sortField',location.href)&&((self.getHrefField('sortType',location.href))=="desc")){
                        sort_Type="asc";
                        URL = self.changeURLPar(location.href,'sortField',sort_Field);
                        url = self.changeURLPar(URL,'sortType',sort_Type);
                    }else if(self.getHrefField('sortField',location.href)&&((self.getHrefField('sortType',location.href))=="asc")){
                        sort_Type="desc";
                        URL = self.changeURLPar(location.href,'sortField',sort_Field);
                        if(self.getHrefField('sortField')==sort_Field){
                            url = self.changeURLPar(URL,'sortType',sort_Type);
                        }else{
                            url=URL;
                        }
                    }else{
                        sort_Type="asc";
                        url = location.href+'&'+'sortField='+sort_Field+'&sortType='+sort_Type;
                    }
                }else{
                    sort_Type="asc";
                    url = location.href+'?'+'sortField='+sort_Field+'&sortType='+sort_Type;
                }
                location.href=url;


            });
            //事件点击
             $m.page.on ('click', 'a',function (e){
     
                var oAlink = $m.page[0].getElementsByTagName("a");
                var inx = $m.curpage; //初始的页码
                var con = $m.page[0];
                var count = $m.countpage;
                oAlink[0].onclick = function() { //点击上一页
                  if (inx == 1) {
                    return false;
                  }
                  inx--;
                  setPage(con, count, inx);
                  return false;
                }

                    inx = parseInt(this.innerHTML);
                    self.setPage(con, count, inx);

                oAlink[oAlink.length - 1].onclick = function() { //点击下一页
                  if (inx == count) {
                    return false;
                  }
                  inx++;
                  setPage(con, count, inx);
                  return false;
                }
                //var locationUrl=location.href.indexOf("?");
                var url="";

                if(self.getHrefField('pageNum',location.href)){
                    url = self.changeURLPar(location.href,'pageNum',inx);
                }else{
                    url = location.href+'&'+'pageNum='+inx;
                }
                location.href=url
          });

        },


        //渲染
        render : function(obj) {
            var self = this;
            self.getAccountNum();
      
        }
        };

        $c.init();
});