// 依赖gb.js
$(function () {
    // 获取当前用户信息,只有用户登陆并且已经是cps用户才请求列表
    //common.getCpsUserInfo(function (cbData) {

        var $m = {
            //分类
            cat : $('#cat'),
            catTpl : $('#catTpl').html(),
            //时间
            datepicker1 : $("#J_datepicker1"),
            datepicker2 : $("#J_datepicker2"),

            //文章库
            hotGoods : $("#J_goods"),
            hotGoodsTpl : $("#goodsTpl").html(),
            hotGoodsItemTpl : $("#goodListTpl").html(),
            goodsBox : $("#J_goodsBox"),

            //公众号库
            hotShops : $("#hotShops"),
            goodsMain : $('#J_goodsMain'),
            numListTpl : $('#numListTpl').html(),
            //搜索
            searchForm : $('#J_searchForm'),
            keyword: $('#keyword'),

            //筛选
            filterCategory : $('#J_filterCategory'),
            lastCategory : $(), //存储最后选择的分类
            filterItems : $('#J_filterItems'),
            //排序
            filterCondition : $('#J_filterCondition'),
            filter : $('#J_filter'),
            filterDate : $('#J_filterDate'),
            //数据
            pageNum : 1,
            pagesize : 10,   //每页加载数量
            curpage : 1,
            countpage : 1,
            //分页
            page : $('#J_page'),
            
        };


        var $c = {

            //初始化
            init : function () {
                var self = this;
                type = common.getQueryString("type");

                $(document).lazyload();

               
                  switch (type) {
                    case "article":
                        $m.lastContentList = $('#J_goodsMain');
                        self.render($m.hotGoods);
                        break;
                    case "account":
                        $m.lastContentList = $('#J_goodsMain');
                        self.render($m.hotShops);
                        break;
                    default:
                        $m.lastContentList = $('#J_goodsMain');
                        self.render($m.hotGoods);
                        break;
                }
                self.bindEvent();

            },

            //加载数据
            getItem : function (postData,_datatype) {
                var self = this;

                // //锁住请求，避免重复请求
                if ( $m.status === true ) {
                    return ;
                }
                var key_word = self.getHrefField('keyword',location.href);
                $m.keyword.attr("value",key_word);
                var key_wordType=postData.keywordType;
                 $m.status = true;
                     $.ajax({
                        url: "/weixin/search",
                        type: "POST",
                        data: postData,
                        dataType: "jsonp",
                        crossDomain : true,
                        success: function (redata, status, xhr) {
                             //解锁请求
                        $m.status = false;
                        var totalNum;
                        if (redata.retCode == 0) {
                            var items = redata.data.list;
                             totalNum = redata.data.totalNum;
                             $m.countpage = Math.ceil(totalNum / 10);
                            var tmplData = [];
                            var list = redata.data.list;
                            for (var i = 0; i < list.length; i++){
                                var time = parseInt(list[i]['article_publish_time']);
                                var d = new Date(time*1000);
                                var article_publish_time = d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate()+' '+d.getHours()+':'+d.getMinutes();
                                list[i]['article_publish_time'] = article_publish_time;
                            }
                        }
                          $m.curpage = postData.pageNum;

                            tmplData = { itemlist:list };
                            if(postData.keywordType==1){
                                $("#g_word").attr("checked",true);
                                $("#g_num").attr("checked",false);
                            //时间排序
                            if ($m.filterCategory.hasClass('on')) {
                                return ;
                            }
                            var $cc = $m.filterCategory;
                            var curObj = $m.cat.children();
                            $curdId = curObj.children("a");

                            if ($curdId.hasClass('on')) {
                                return ;
                            }
                            $m.filterCategory.find('a').removeClass("on");

                            var datevalue= postData.period;

                            switch (datevalue) {
                                case "3":
                                    $($curdId[1]).addClass("on");
                                    break;

                                case "7":
                                    $($curdId[2]).addClass("on");
                                    break;

                                case "15":
                                    $($curdId[3]).addClass("on");
                                    break;

                                case "30":
                                    $($curdId[4]).addClass("on");
                                    break;
                                default:
                                $($curdId[0]).addClass("on");
                                break;
                            }

                                var render = _datatype === undefined ? Mustache.render($m.hotGoodsTpl, tmplData) : Mustache.render($m.hotGoodsItemTpl, tmplData);
                                typeof postData.callback === "function" ? postData.callback(render) : $m.hotGoods.find('.list_bd').append(render);
                                self.setPage($m.page[0],$m.countpage,$m.curpage);
                                $(document).lazyload('load');


                                if (totalNum < 1)
                                { // 没有数据则提示没有数据
                                    $m.hotGoods.html('<div class="dm_mod_tips"><p>没有搜索到指定商品</p></div>');
                                }

                                //var isLoaded = postData.startpage >= countPage; // 所有分页加载完毕
                                var isLoaded = true; // 所有分页加载完毕
                                if (totalNum < 1 || isLoaded) {
                                    // 没有数据或所有分页加装完毕 清空加装提示
                                    $m.hotGoods.children(".global-loading").remove();
                                    $m.hotGoods.children(".load_more").remove();
                                }
                            }else{
                                $("#g_word").attr("checked",false);
                                $("#g_num").attr("checked",true);
                                $m.cat.hide();
                                var render = _datatype === undefined ? Mustache.render($m.numListTpl, tmplData) : Mustache.render($m.hotGoodsItemTpl, tmplData);
                                typeof postData.callback === "function" ? postData.callback(render) : $m.hotShops.find('.list_bd').append(render);
                                self.setPage($m.page[0],$m.countpage,$m.curpage);
                                $(document).lazyload('load');
                                  if (totalNum < 1)
                                { // 没有数据则提示没有数据
                                    $m.hotShops.html('<div class="dm_mod_tips"><p>没有搜索到指定商品</p></div>');
                                }

                                var isLoaded = true; // 所有分页加载完毕
                                if (totalNum < 1 || isLoaded) {
                                    // 没有数据或所有分页加装完毕 清空加装提示
                                    $m.hotShops.children(".global-loading").remove();
                                    $m.hotShops.children(".load_more").remove();
                                    $m.hotGoods.children(".global-loading").remove();
                                    $m.hotGoods.children(".load_more").remove();
                                }
                            }
                            }
                        })

                    },
            //取出url参数
            getHrefField :function (field, href) {
                var ret = "";
                var href = href || location.href;
                var fieldStr, fieldArr, index;
                if (href.indexOf("?") !== -1) {
                    fieldStr = href.substring(href.indexOf("?") + 1);
                    fieldArr = fieldStr.split("&");
                    for (var i = 0, len = fieldArr.length; i < len; i++) {
                        index = fieldArr[i].indexOf(field + "=");
                        if (index !== -1) {
                            ret = fieldArr[i].substring(index + (field + "=").length);
                            break;
                        }
                    }

                    return decodeURIComponent(ret);
                } else {
                    return decodeURIComponent(ret);
                }
            },
            changeURLPar :function (url, ref, value) {
                var str = "";
                if (url.indexOf('?') != -1)
                    str = url.substr(url.indexOf('?') + 1);
                else
                    return url + "?" + ref + "=" + value;
                var returnurl = "";
                var setparam = "";
                var arr;
                var modify = "0";

                if (str.indexOf('&') != -1) {
                    arr = str.split('&');
                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i].split('=')[0] == ref) {
                            setparam = value;
                            modify = "1";
                        }
                        else {
                            setparam = arr[i].split('=')[1];
                        }
                        returnurl = returnurl + arr[i].split('=')[0] + "=" + setparam + "&";
                    }
                    returnurl = returnurl.substr(0, returnurl.length - 1);
                    if (modify == "0")
                        if (returnurl == str)
                            returnurl = returnurl + "&" + ref + "=" + value;
                }
                else {
                    if (str.indexOf('=') != -1) {
                        arr = str.split('=');
                        if (arr[0] == ref) {
                            setparam = value;
                            modify = "1";
                        }
                        else {
                            setparam = arr[1];
                        }
                        returnurl = arr[0] + "=" + setparam;
                        if (modify == "0")
                            if (returnurl == str)
                                returnurl = returnurl + "&" + ref + "=" + value;
                    }
                    else
                        returnurl = ref + "=" + value;
                }
                return url.substr(0, url.indexOf('?')) + "?" + returnurl;
            },
            setPage :function(con, count, pageindex) {
                var con = con;
                var count = count;
                var pageindex = pageindex;
                var a = [];
                  //总页数少于10 全部显示,大于10 显示前3 后3 中间3 其余....
                  if (pageindex == 1) {
                    a[a.length] = "<a href=\"#\" class=\"prev unclick\">prev</a>";
                  } else {
                    a[a.length] = "<a href=\"#\" class=\"prev\">prev</a>";
                  }
                  function setPageList() {
                    if (pageindex == i) {
                      a[a.length] = "<a href=\"#\" class=\"on\">" + i + "</a>";
                    } else {
                      a[a.length] = "<a href=\"#\">" + i + "</a>";
                    }
                  }
                  //总页数小于10
                  if (count <= 10) {
                    for (var i = 1; i <= count; i++) {
                      setPageList();
                    }
                  }
                  //总页数大于10页
                  else {
                    if (pageindex <= 4) {
                      for (var i = 1; i <= 5; i++) {
                        setPageList();
                      }
                      a[a.length] = "...<a href=\"#\">" + count + "</a>";
                    } else if (pageindex >= count - 3) {
                      a[a.length] = "<a href=\"#\">1</a>...";
                      for (var i = count - 4; i <= count; i++) {
                        setPageList();
                      }
                    }
                    else { //当前页在中间部分
                      a[a.length] = "<a href=\"#\">1</a>...";
                      // alert(pageindex-2);
                      // alert(pageindex+2);
                      for (var i = (pageindex-2); i <= (parseInt(pageindex)+2); i++) {
                        setPageList();
                      }
                      a[a.length] = "...<a href=\"#\">" + count + "</a>";
                    }
                  }
                  if (pageindex == count) {
                    a[a.length] = "<a href=\"#\" class=\"next unclick\">next</a>";
                  } else {
                    a[a.length] = "<a href=\"#\" class=\"next\">next</a>";
                  }
                  con.innerHTML = a.join("");
                },
    

            //绑定事件
            bindEvent : function () {

                var self = this;

                //列表排序
                $m.hotGoods.on('click', '#J_filterCondition p', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var curId = this.id;
                    var sort_Field,
                        sort_Type = "";
                    switch (curId) {
                        case "J_filterDate":
                            sort_Field="date";
                            break;
                        case "J_filterRead":
                            sort_Field="read";
                            break;
                        case "J_filterGoods": //
                            sort_Field="like";
                            break;
                        default:
                            break;
                    }
                    var locationUrl=location.href.indexOf("?");
                    var url="";
                    var URL="";
                    if(locationUrl>0){
                        if(self.getHrefField('sortField',location.href)&&((self.getHrefField('sortType',location.href))=="desc")){
                            sort_Type="asc";
                            URL = self.changeURLPar(location.href,'sortField',sort_Field);
                            url = self.changeURLPar(URL,'sortType',sort_Type);
                        }else if(self.getHrefField('sortField',location.href)&&((self.getHrefField('sortType',location.href))=="asc")){
                            sort_Type="desc";
                            URL = self.changeURLPar(location.href,'sortField',sort_Field);
                            if(self.getHrefField('sortField')==sort_Field){
                                url = self.changeURLPar(URL,'sortType',sort_Type);
                            }else{
                                url=URL;
                            }
                        }else{
                            sort_Type="asc";
                            url = location.href+'&'+'sortField='+sort_Field+'&sortType='+sort_Type;
                        }
                    }else{
                        sort_Type="asc";
                        url = location.href+'?'+'sortField='+sort_Field+'&sortType='+sort_Type;
                    }
                    location.href=url;


                });
                //时间分类
               $m.filterCategory.on("click", "li", function (e)  {

                    e.preventDefault();
                    e.stopPropagation();

                    var $this = $(this);

                    var curId = this.id;
                    var curObj = $(this),
                        _period=0;

                    switch (curId) {
                        case "J_three":
                            _period=3;
                            break;
                        case "J_seven":
                            _period=7;
                            break;
                        case "J_fifteen": 
                            _period=15;
                            break;
                        case "J_thirty":
                         _period=30;
                          break;
                        default:
                            break;
                    }
                    var locationUrl=location.href.indexOf("?");
                    var url="";
                    if(locationUrl>0){
                        if(self.getHrefField('period',location.href)){
                            url = self.changeURLPar(location.href,'period',_period);
                        }else{
                            url = location.href+'&'+'period='+_period;
                        }
                        
                    }else{
                        url = location.href+'?'+'period='+_period;
                    }
                    location.href=url;
                    });

                //点击文章或者公众号筛选器
                $m.filter.on('click', 'input',function (e) {
                     var checkedId = $('input[type=radio][checked]').val();
                     var $this = $(this);
                      var idx = $m.filter.find('input').index( $this );
                        $('.wrapper').hide().eq(idx).show();
                         if(checkedId==1){//点击公众号
                            $("#g_word").attr("checked",false);
                            $("#g_num").attr("checked",true);
                            $m.cat.hide();
                            $m.page.hide();
                            var locationUrl=location.href.indexOf("?");
                                if(locationUrl>0&&$.trim($("#keyword").val())){
                                    if(self.getHrefField('keyword',location.href)&&self.getHrefField('keywordType',location.href)){
                                        var url = self.changeURLPar(location.href,'keywordType',2);
                                        location.href = url;
                                    }else{
                                        var url = location.href+'&'+'keywordType=2';
                                        location.href = url;
                                    }
                                }
                         }else{//再次点击文章
                            $m.cat.show();
                            $m.page.show();
                            $("#g_num").attr("checked",false);
                            $("#g_word").attr("checked",true);
                            if(self.getHrefField('keywordType',location.href)==2){
                                var url = self.changeURLPar(location.href,'keywordType', 1);
                                location.href = url;
                            }else if(self.getHrefField('keywordType',location.href)==1){
                                var url = self.changeURLPar(location.href,'keywordType', 2);
                                location.href = url;
                            }else{
                                 var data = {
                                    sortType:self.getHrefField('sortType',location.href)||"desc", //
                                        sortField: self.getHrefField('sortField',location.href)||"date", //
                                        keyword:""||encodeURIComponent($.trim($("#keyword").val())) ,
                                        keywordType:1,
                                        pageNum: 1, //支持分页，0为第一页
                                        pageSize: 10, //每页记录数
                                        callback: function (render) {
                                            $m.hotGoods.children(".global-loading").before(render);
                                            $(document).lazyload('load');
                                        }
                                };
                                self.getItem(data);
                            }
                    }
                });
                 //事件点击
                 $m.page.on ('click', 'a',function (e){
         
                    var oAlink = $m.page[0].getElementsByTagName("a");
                    var inx = $m.curpage; //初始的页码
                    var con = $m.page[0];
                    var count = $m.countpage;
                    oAlink[0].onclick = function() { //点击上一页
                      if (inx == 1) {
                        return false;
                      }
                      inx--;
                      setPage(con, count, inx);
                      return false;
                    }

                        inx = parseInt(this.innerHTML);
                        self.setPage(con, count, inx);

                    oAlink[oAlink.length - 1].onclick = function() { //点击下一页
                      if (inx == count) {
                        return false;
                      }
                      inx++;
                      setPage(con, count, inx);
                      return false;
                    }
                    var locationUrl=location.href.indexOf("?");
                    var url="";

                    if(locationUrl>0){
                        if(self.getHrefField('pageNum',location.href)){
                            url = self.changeURLPar(location.href,'pageNum',inx);
                        }else{
                            url = location.href+'&'+'pageNum='+inx;
                        }
                        
                    }else{
                        url = location.href+'?'+'pageNum='+inx;
                    }
                    location.href=url
              });
                // 搜索
                $m.searchForm.submit(function (e) {
                        e.preventDefault();
                        e.stopPropagation();

                        $("#keyword").trigger("blur");

                        // if ( !$.trim($("#keyword").val()) ) {
                        //     return ;
                        // }

                        $m.hotGoods.html(common.loadingHtml);

                        // if ( $m.isSearching ) {
                        //     return ;
                        // }

                        //$m.isSearching = true;
                        var key_wordType;
                        var checkedId = $('input[type=radio][checked]').val();
                        //alert(checkedId);
                        if(checkedId==1){
                            key_wordType=1;
                            var _data = {
                                sortType:self.getHrefField('sortType',location.href)||"desc", //
                                sortField:self.getHrefField('sortField',location.href)||"date", //
                                keyword:encodeURIComponent($.trim($("#keyword").val())),
                                pageNum: self.getHrefField('pageNum',location.href)||1, //支持分页，0为第一页
                                pageSize: self.getHrefField('pageSize',location.href)||10, //每页记录数
                                keywordType:key_wordType,
                                callback: function (render) {
                                    $m.hotGoods.children(".global-loading").before(render);
                                    $(document).lazyload('load');
                                }
                            };
                        }else{
                            key_wordType=2;
                            var _data_account = {
                                keyword:encodeURIComponent($.trim($("#keyword").val())),
                                pageNum: self.getHrefField('pageNum',location.href)||1, //支持分页，0为第一页
                                pageSize: self.getHrefField('pageSize',location.href)||10, //每页记录数
                                keywordType:key_wordType,
                                callback: function (render) {
                                    $m.hotShops.children(".global-loading").before(render);
                                    $(document).lazyload('load');
                                }
                            };

                        }
                        
                        var locationUrl=location.href.indexOf("?");
                        var url="";
                        var key_word=encodeURIComponent($.trim($("#keyword").val()));

                        if(locationUrl>0){
                            if(self.getHrefField('keyword',location.href)){
                                url = self.changeURLPar(location.href,'keyword',key_word);
                            }else{
                                url =location.href+'&'+'keyword='+key_word+'&keywordType='+key_wordType;
                            }
                            
                        }else{
                            url =location.href+'?'+'keyword='+key_word+'&keywordType='+key_wordType;
                        }
                       location.href=url;
                    });

            },



            //渲染
            render : function(obj) {
                var self = this;
                //文章库
                // 切换文章库
                switch (obj) {
                //文章库
                    case $m.hotGoods :
                    $m.hotGoods.append(common.loadingHtml);
                    $m.cat.show();
                    var data = {
                        sortType:self.getHrefField('sortType',location.href)||"desc", //
                        sortField: self.getHrefField('sortField',location.href)||"date", //
                        keyword:self.getHrefField('keyword',location.href)||"",
                        keywordType:self.getHrefField('keywordType',location.href)||1,
                        pageNum: self.getHrefField('pageNum',location.href)||1, //支持分页，0为第一页
                        pageSize: self.getHrefField('pageSize',location.href)||10, //每页记录数
                        period: self.getHrefField('period',location.href)||0,
                        callback: function (render) {
                            $m.hotGoods.children(".global-loading").before(render);
                            $(document).lazyload('load');
                             //列表排序
                            var orderType = data.sortType;//desc/asc
                            var fieldOrderType =data.sortField;//date/read/like
                            var url = location.href;

                            switch (fieldOrderType) {
                                case "date":
                                if(orderType=="asc"){
                                    $m.hotGoods.find('p').removeClass('on');
                                    $m.hotGoods.find('#J_filterDate').addClass('on');
                                    $m.hotGoods.find('#J_filterDate').children("i").toggleClass("dm_mod_filter_arrow_down");
                                }
                                break;
                                case "read":
                                if(orderType=="asc"){
                                    $m.hotGoods.find('p').removeClass('on');
                                    $('#J_filterRead').addClass('on');
                                    $('#J_filterRead').children("i").toggleClass("dm_mod_filter_arrow_down");
                                }
                                break;
                                case "like":
                                if(orderType=="asc"){
                                    $m.hotGoods.find('p').removeClass('on');
                                    $('#J_filterGoods').addClass('on');
                                    $('#J_filterGoods').children("i").toggleClass("dm_mod_filter_arrow_down");
                                }
                                break;
                                default:
                                break;
                                }
                            }
                    };
                        self.getItem(data);
                        break;
                         //公众号
                        case $m.hotShops :
                            // 切换公众号
                            $m.hotShops.append(common.loadingHtml);
                            $m.cat.hide();
                            var data = {
                                keyword:self.getHrefField('keyword',location.href)||" ",
                                keywordType:self.getHrefField('keywordType',location.href)||2,
                                pageNum: self.getHrefField('pageNum',location.href)||1, //支持分页，0为第一页
                                pageSize: self.getHrefField('pageSize',location.href)||10, //每页记录数
                                callback: function (render) {
                                    $m.hotShops.children(".global-loading").before(render);
                                    $(document).lazyload('load');
                                    }
                            };
                            self.getItem(data);
                            break;
                    }
            }
        };

        $c.init();
});