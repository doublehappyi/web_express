/**
 * Created by yishuangxi on 2015/10/19.
 */
var mysql = require('mysql');
var helper = require('../helper');
var config = require('../config');
var swig = require('swig');
var db = helper.db;
var logger = helper.logger;
var article = config.db.table.article;
var weixin = config.db.table.weixin;

//文章搜索
exports.searchArticles = function (query, suc_cb, err_cb) {
    logger.debug('searchArticles');
    var filer_str = swig.compile('where {{article}}.article_title regexp ? ' +
            'and {{article}}.article_publish_time>? and {{article}}.article_publish_time<? ' +
            'and {{article}}.ref_wx_id={{weixin}}.wx_id ' +
            'order by {{article}}.?? ' + query.sortType+' ')({article:article, weixin:weixin }),
        limit_str = 'limit ?, ?',
        select_str = swig.compile('select ' +
            '{{article}}.article_id, {{article}}.article_title, {{article}}.article_url, {{article}}.article_read_num, ' +
            '{{article}}.article_like_num, {{article}}.article_publish_time, ' +
            '{{weixin}}.wx_id, {{weixin}}.wx_name, {{weixin}}.wx_account, {{weixin}}.wx_intro, {{weixin}}.wx_qrcode ' +
            'from {{article}}, {{weixin}} ' + filer_str)({article:article, weixin:weixin });
    var page_str = select_str + limit_str,
        page_str = mysql.format(page_str, [query.keyword, query.startPublishTime, query.endPublishTime, query.sortField, query.pageSize * (query.pageNum - 1), query.pageSize]);

    var count_str = swig.compile('select count({{article}}.article_id) as totalNum from {{article}}, {{weixin}} ' + filer_str)({article:article, weixin:weixin }),
        count_str = mysql.format(count_str, [query.keyword, query.startPublishTime, query.endPublishTime, query.sortField]);
    var sql =  count_str + ';' + page_str;
    logger.debug('searchArticles: sql:'+sql);
    db.getConnection(function (err, conn) {
        if (err) throw err;
        conn.query(sql, function (err, rows, fields) {
            if (err && typeof err_cb === 'function') err_cb(err);
            if (typeof suc_cb === 'function') suc_cb(rows, fields);
        });
        conn.release();
    });
};

//微信搜索
exports.searchWeixin = function (data, suc_cb, err_cb) {
    var filter_str = 'where wx_name regexp ? ',
        limit_str = 'limit ?, ?';
    var page_str = swig.compile('select wx_id, wx_name, wx_account, wx_intro, wx_qrcode from {{weixin}} ' + filter_str + limit_str)({weixin:weixin}),
        page_str = mysql.format(page_str, [data.keyword, data.pageSize * (data.pageNum - 1), data.pageSize]);
    var count_str = swig.compile('select count(wx_id) as totalNum from {{weixin}} ' + filter_str)({weixin:weixin}),
        count_str = mysql.format(count_str, [data.keyword]);
    var sql = count_str + ';' + page_str;
    logger.debug('searchWeixin: sql:'+sql);
    db.getConnection(function (err, conn) {
        if (err) throw err;
        conn.query(sql, function (err, rows, fields) {
            if (err && typeof err_cb === 'function') err_cb(err);
            if (typeof suc_cb === 'function') suc_cb(rows, fields);
        });
        conn.release();
    });
};

//指定微信号下的文章查询
exports.retrieveArticles = function(query, suc_cb, err_cb){
    //三个数据库的搜索查询语句：
    // 1，文章列表查詢
    // 2，文章总数查询
    // 3，微信号详细信息查询
    //, wx_intro, wx_qrcode
    var filter_str = swig.compile('where ref_wx_id=? order by {{article}}.?? ' + query.sortType+' ')({article:article}),
        limit_str = 'limit ?, ?';

    var select_str = swig.compile('select article_id, article_title, article_url, article_read_num, article_like_num, article_publish_time from {{article}} ')({article:article}),
        page_str = select_str + filter_str + limit_str,
        page_str = mysql.format(page_str, [query.wx_id, query.sortField, query.pageSize * (query.pageNum - 1), query.pageSize]);

    var select_str = swig.compile('select count(article_id) as totalNum from {{article}} ')({article:article}),
        count_str = select_str + filter_str,
        count_str = mysql.format(count_str, [query.wx_id, query.sortField]);

    var select_str = swig.compile('select wx_id, wx_name, wx_account, wx_intro, wx_qrcode from {{weixin}} ')({weixin:weixin}),
        filter_str = 'where wx_id=? ',
        wx_str = select_str + filter_str,
        wx_str = mysql.format(wx_str, [query.wx_id]);

    var sql = page_str + ';' + count_str + ';' + wx_str;
    logger.debug('retrieveArticles: sql:'+sql);

    db.getConnection(function (err, conn) {
        if (err) throw err;
        conn.query(sql, function (err, rows, fields) {
            if (err && typeof err_cb === 'function') err_cb(err);
            if (typeof suc_cb === 'function') suc_cb(rows, fields);
        });
        conn.release();
    });
};
