module.exports = (function () {
    var log4js = require('log4js');
    log4js.configure({
        appenders: [
            { type: 'console' },
            { type: 'file', filename: 'logs/weixin.log', category: 'weixin' }
        ]
    });
    var logger = log4js.getLogger('weixin');
    logger.setLevel('DEBUG');

    return logger;
})();