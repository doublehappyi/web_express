var helper = require('../helper');
var logger = helper.logger;

exports.validateSearchQuery = function (req, res, next) {
    var query = req.query,
        maxPageSize = 50;
    var sortFieldMap = {
        'date': 'article_publish_time',
        'read': 'article_read_num',
        'like': 'article_like_num'
    };
    var maxPeriod = 1000;//这个1000是天数，为最大时间跨度
    var sortField = ['date', 'read', 'like'].indexOf(query.sortField) < 0 ? 'date' : query.sortField;
    var keywordType = parseInt(query.keywordType);

    query.keyword = decodeURIComponent(query.keyword) || ".";
    query.pageSize = !query.pageSize || (query.pageSize && parseInt(query.pageSize) > maxPageSize) ? 10 : parseInt(query.pageSize);
    query.pageNum = parseInt(query.pageNum) || 1;

    query.keywordType = [1, 2].indexOf(keywordType) < 0 ? 1 : keywordType;
    //如果是文章搜索，则还需要处理以下查询词
    if (query.keywordType === 1){
        var period = [0, 3, 7, 15, 30].indexOf(parseInt(query.period)) <= 0 ? maxPeriod : parseInt(query.period);
        query.sortType = ['desc', 'asc'].indexOf(query.sortType) < 0 ? 'desc' : query.sortType;
        query.sortField = sortFieldMap[sortField];
        query.endPublishTime = parseInt(new Date().getTime() / 1000);
        query.startPublishTime = query.endPublishTime - period * 24 * 60 * 60;
    }
    logger.debug('validateSearchQuery: query: '+JSON.stringify(query));
    next();
};

exports.validateTargetArticlesQuery = function (req, res, next) {
    if(!parseInt(req.query.wxId)){
        res.json({retCode:1, msg:'wxId is invalid'});
    }
    var query = req.query;
    var maxPageSize = 50;
    var sortFieldMap = {
        'date': 'article_publish_time',
        'read': 'article_read_num',
        'like': 'article_like_num'
    };
    var sortField = ['date', 'read', 'like'].indexOf(query.sortField) < 0 ? 'date' : query.sortField;
    query.wx_id = parseInt(query.wxId);
    query.sortType = ['desc', 'asc'].indexOf(query.sortType) < 0 ? 'desc' : query.sortType;
    query.sortField = sortFieldMap[sortField];
    query.pageSize = !query.pageSize || (query.pageSize && parseInt(query.pageSize) > maxPageSize) ? 10 : parseInt(query.pageSize);
    query.pageNum = parseInt(query.pageNum) || 1;

    next();
};