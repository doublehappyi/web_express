var router = require('express').Router();
var middlewares = require('../middlewares');
var models = require('../models');
var helper = require('../helper');
var logger = helper.logger;
var validate = middlewares.validate;

//微信搜索页
router.get('/', function (req, res, next) {
    res.render('weixin/index.html');
});

//指定微信号下的文章列表页面
router.get('/list', function (req, res, next) {
    res.render('weixin/list.html');
});

//微信、文章搜索接口
router.get('/search', validate.validateSearchQuery, function (req, res, next) {
    logger.debug('/search ...');
    var keywordType = req.query.keywordType;
    if (keywordType === 1) {
        logger.debug('keywordType === 1');
        models.weixin.searchArticles(req.query, function suc_cb(results) {
            res.jsonp({retCode: 0, msg: '', data: {totalNum: results[0][0].totalNum, list: results[1]}});
        }, dbErr);
    } else {
        logger.debug('keywordType === 2');
        models.weixin.searchWeixin(req.query, function suc_cb(results) {
            res.jsonp({retCode: 0, msg: '', data: {totalNum: results[0][0].totalNum, list: results[1]}});
        }, dbErr);
    }
});

//指定微信号下的文章列表接口
router.get('/articles', validate.validateTargetArticlesQuery, function (req, res, next) {
    models.weixin.retrieveArticles(req.query, function suc_cb(results) {
        var articleListData = results[0],
            totalNumListData = results[1],
            wxListData = results[2];
        var data = wxListData[0];
        data.totalNum = totalNumListData[0].totalNum;
        data.list = articleListData;
        res.jsonp({retCode: 0, msg: '', data: data});
    }, dbErr);
});

function dbErr(err) {
    logger.err('dbErr: ' + JSON.stringify(err));
    res.jsonp({retCode: 1, msg: 'mysql error !'});
}

module.exports = router;
