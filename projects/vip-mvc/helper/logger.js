module.exports = (function () {
    var log4js = require('log4js');
    log4js.configure({
        appenders: [
            { type: 'console' },
            { type: 'file', filename: 'logs/vip.log', category: 'vip' }
        ]
    });
    var logger = log4js.getLogger('vip');
    logger.setLevel('DEBUG');
    
    return logger;
})();