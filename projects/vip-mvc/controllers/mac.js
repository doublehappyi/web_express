var router = require('express').Router();
var middlewares = require('../middlewares');
var models = require('../models');
var helper = require('../helper');
var validate = middlewares.validate;
var logger = helper.logger;

router.post('/mac', validate.macValidate, function(req, res, next){
    var mac = { macAddr:req.body.macAddr };
    logger.debug('mac: '+JSON.stringify(mac));
    models.mac.retrieve(
        mac,
        function suc_cb(rows, fields){
            if (rows.length > 0){
                res.json({retCode:1, msg:'macAddr' + mac.macAddr+'已经被注册'});
            }else{
                models.mac.register(
                    mac,
                    function suc_cb(result){
                        res.json({retCode:0, msg:'macAddr register ok', data:{id:result.insertId}});
                    },
                    function err_cb(err) {
                        res.json({retCode:1, msg:'mysql register error'});
                    });
            }
        },
        function err_cb(err){
            res.json({retCode:1, msg:'mysql retrieve error'});
        }); 
});

router.get('/mac', validate.macValidate, function(req, res, next){
    var mac = {macAddr:req.query.macAddr}
    models.mac.retrieve(mac, function suc_cb(rows, fields) {
        if (rows.length > 0){
            res.json({retCode:0, msg:'ok', data:{id: rows[0].id}}); 
        }else{
            res.json({retCode:1, msg:'macAddr ' + req.query.macAddr + '不在数据库中！'}); 
        }
        
    },function err_cb(err) {
        res.json({retCode:1, msg:'mysql retrieve error'});
    });
});

module.exports = router;
