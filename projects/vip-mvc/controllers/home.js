var router = require('express').Router()

router.get('/', function home(req, res, next) {
    res.render('home') 
});

module.exports = router;