var router = require('express').Router();
var middlewares = require('../middlewares');
var models = require('../models');
var validate = middlewares.validate;

router.get('/register', function(req, res, next){
    res.render('register.html');
});

router.get('/login', function(req, res, next){
    res.render('login.html');
});

router.post('/register', validate.phoneValidate, validate.passwordValidate, function(req, res, next){
    models.user.retrieve(
        { phone:req.body.phone }, 
        function suc_cb(rows, fields){
            if (rows.length > 0){
                res.json({retCode:1, msg:'手机号码'+req.body.phone+'已经被注册！'});
            }else{
                models.user.register(
                    { username:req.body.phone, password: req.body.password, phone: req.body.phone}, 
                    function suc_cb(result){
                        res.json({retCode:0, msg:'注册成功！', data:{id:result.insertId}});
                    }, 
                    function err_cb(err){
                        res.json({retCode:1, msg:'mysql err register'});
                    }); 
            }
        },
        function err_cb(err){
            res.json({retCode:1, msg:'mysql err retrieve'});
        });
});

router.post('/login', validate.usernameValidate, validate.passwordValidate ,function(req, res, next){
    models.user.login(
        {username:req.body.username, password: req.body.password }, 
        function suc_cb(rows, fields){
            if (rows.length > 0){
                res.json({retCode:0, msg:'ok', data:{id:rows[0].id}});
            }else{
                res.json({retCode:1, msg:'登录失败，用户名或密码错误!'});
            }
        }, 
        function err_cb(err){
            res.json({retCode:1, msg:'mysql err'});
        });   
});

module.exports = router;