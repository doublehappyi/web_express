var helper = require('../helper');
var db = helper.db;
var models = require('../models');
var logger = helper.logger;

exports.phoneValidate = function(req, res, next){
    var phone = req.body.phone;
    logger.debug('middlewares/validate.js/phoneValidate:phone:' + phone);
    if (!phone || phone.length !=11 || !parseInt(phone) ){
        res.json({retCode:1, msg:'phone is invalid'});
    }else{
        next();
    }
};

exports.usernameValidate = function(req, res, next){
    var username = req.body.username;
    if (!username){
        res.json({retCode:1, msg:'username is invalid'});
    }else{
        next();
    }
}

exports.passwordValidate = function(req, res, next){
    var password = req.body.password;
    logger.debug('password: '+password);
    if (!password || password.length > 20 || password.length < 6){
        res.json({retCode:1, msg:'password is invalid'});
    }else{
        next();
    }
}

exports.macValidate = function(req, res, next){
    var macAddr = req.body.macAddr || req.query.macAddr;
    var r_macAddr = /^([1-9a-eA-E]{2}-){5}[1-9a-eA-E]{2}$/;
    logger.debug('macAddr: '+macAddr);
    if (!macAddr || !r_macAddr.test(macAddr)){
        res.json({retCode:1, msg:'macAddr is invalid'});
    }else{
        next();
    }
}

exports.idValidate = function(req, res, next){
    var id = req.body.id || req.query.id;
    if (!id || !parseInt(id)){
        res.json({retCode:1, msg:'id is invalid'});
    }else{
        next();
    }
}