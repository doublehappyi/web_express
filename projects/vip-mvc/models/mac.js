var mysql = require('mysql');
var helper = require('../helper');
var config = require('../config');
var db = helper.db;
var logger = helper.logger;
var swig = require('swig');

var tb_mac = config.db.table.mac;

exports.retrieve = function(mac, suc_cb, err_cb){
    var sql = swig.compile('select id from {{mac}} where macAddr="{{macAddr}}"')({mac:tb_mac, macAddr:mac.macAddr});
    logger.debug('mac retrive sql: '+sql);
    db.getConnection(function(err, conn){
        conn.query(sql, function(err, rows, fields){
            if (err && typeof err_cb === 'function') err_cb(err);
            if (typeof suc_cb === 'function') suc_cb(rows, fields);
        });
        conn.release();
    });
}

exports.register = function(mac, suc_cb, err_cb){
    var sql = swig.compile('insert into {{mac}} values(null, "{{macAddr}}", {{datetime}})')({mac:tb_mac, macAddr:mac.macAddr, datetime:parseInt(new Date().getTime()/1000)});
    logger.debug('mac register sql: '+sql);
    db.getConnection(function(err, conn){
        conn.query(sql, function(err, result){
            if (err && typeof err_cb === 'function') err_cb(err);
            if (typeof suc_cb === 'function') suc_cb(result);
        });
        conn.release();
    });
}