var mysql = require('mysql');
var helper = require('../helper');
var db = helper.db;
var logger = helper.logger;


exports.retrieve = function(user, suc_cb, err_cb){
    var values = [],
        sql_str = 'select * from user where ';
    logger.debug('models/user.js:user: ' + JSON.stringify(user));
    var isFirstTime = true;
    for (var field in user){
        sql_str = sql_str + (isFirstTime ? '':' and ') + field + '=?';
        values.push(user[field]);
        isFirstTime = false;
    }
    logger.debug('models/user.js:sql_str: ' + sql_str);
    var sql = mysql.format(sql_str, values);
    
    logger.debug('models/user.js:retrieve: ' + sql);
    
    db.getConnection(function(err, conn){
        conn.query(sql, function(err, rows, fields){
            if (err && typeof err_cb === 'function') err_cb(err);
            if (typeof suc_cb === 'function') suc_cb(rows, fields);
        });
        conn.release();
    });
};

exports.register = function(user, suc_cb, err_cb){
    var values = [user.username, user.password, user.phone, parseInt(new Date().getTime()/1000)],
        sql_str = 'insert into user (id, username, password, phone, datetime) values (null, ?, ?, ?, ?)';
    var sql = mysql.format(sql_str, values);
    
    db.getConnection(function(err, conn){
        
        conn.query(sql, function(err, result){
            if (err && typeof err_cb === 'function') err_cb(err);
            if (typeof suc_cb === 'function') suc_cb(result);
        });
        conn.release();
    });
};

exports.login = function(user, suc_cb, err_cb){
    var values = [user.username, user.username, user.password],
        sql_str = 'select id from user where (username=? or phone=?) and password=? limit 1';
    var sql = mysql.format(sql_str, values);
    
    logger.debug('exports.login: sql: ' + sql);
    
    db.getConnection(function(err, conn){
        conn.query(sql, function(err, rows, fields){
            if (err && typeof err_cb === 'function') err_cb(err);
            if (typeof suc_cb === 'function') suc_cb(rows, fields);
        });
        conn.release();
    });
};