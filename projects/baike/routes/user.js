var express = require('express');
var router = express.Router();
var helper = require('../helper');
var logger = helper.utils.getLogger();
var mysql = require('mysql');

//登录页
router.get('/login', function (req, res, next) {
    res.render('login.html');
});
//登录
router.post('/login', function (req, res, next) {
    var username = req.body.username;
    var password = req.body.password;
    var pool = req.app.pool;
    sql_str = mysql.format('select * from user where username=? and password=?', [username, password]);
    pool.getConnection(function (err, conn) {
        if (err) throw err;
        conn.query(sql_str, function (err, rows, fields) {
            if (err) throw err;
            if (rows.length) {
                res.send('login success! ' + ' id: ' + rows[0].id + ' username: ' + username + ' password: ' + password)
            } else {
                res.send('login error!');
            }
        });
        conn.release();
    });
});
//注册页
router.get('/register', function (req, res, next) {
    res.render('register.html');
});
//注册
router.post('/register', function (req, res, next) {
    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;

    var pool = req.app.pool;
    pool.getConnection(function (err, conn) {
        if (err) throw err;

        var datetime = Math.round((new Date().getTime())/1000);
        var insert_str = 'insert into user values(null, ?, ?, ?, ?)';
        insert_str = mysql.format(insert_str, [username, password, email, datetime]);
        conn.query(insert_str, function (err, result) {
            if (err) throw err;
            res.send("insertId: " + result.insertId);
        });
    });
});

module.exports = router;