var express = require('express');
var router = express.Router();
var helper = require('../helper');
var logger = helper.utils.getLogger();

router.get('/', function(req, res, next){
    res.render('home.html');
});

module.exports = router;