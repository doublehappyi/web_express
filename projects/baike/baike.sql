#数据库
create database baike;

use baike;

#用户表
CREATE TABLE user (
    id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(20) UNIQUE,
    password VARCHAR(20) NOT NULL,
    email VARCHAR(50) NOT NULL UNIQUE,
    datetime INT(10) NOT NULL
);

#文章表
CREATE TABLE article(
    id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    content VARCHAR(200) NOT NULL,#文章内容
    picture MEDIUMBLOB DEFAULT null,#文章配图
    user_id INT(10) NOT NULL,#文章所有者
    datetime INT(10) NOT NULL
);

#点赞表
CREATE TABLE praise(
    id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    content TINYINT(1) NOT NULL,#值只能为0或者1
    article_id INT(10),#点赞文章
    user_id INT(10),#点赞者
    datetime INT(10) NOT NULL
);

#评论表
CREATE TABLE comment(
    id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    content VARCHAR(200) NOT NULL,#评论内容
    article_id INT(10) NOT NULL,#评论文章
    user_id INT(10) NOT NULL,#评论者
    datetime INT(10) NOT NULL
);