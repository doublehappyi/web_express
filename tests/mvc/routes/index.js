var express = require('express');
var router = express.Router();

module.exports = {
    'home': require('./home'),
    'users': require('./users'),
    'nest.nest': require('./nest/nest')
};
