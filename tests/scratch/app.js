var express = require('express');
var app = express();
var admin = express();
var secret = express();

app.get('/', function (req, res) {
    //res.send('hello app index pages');
    res.format({
        'application/json':function(){
            res.send({hello:'world'});
        }
    });
});


app.get('/user/:id', function(req, res, next){
    res.send('user id is ' + req.params.id);
    res.send('hello');
});

admin.get('/', function (req, res) {
   res.send('hello admin index page: admin.mountpath: ' + admin.mountpath );
});

admin.get('/settings', function(req, res){
   res.send('admin setings');
});

secret.get('/', function(req, res){
    res.send('hello secret index page');
});



admin.use('/secret', secret, function(){
    console.log('');
});
app.use('/admin', admin);

app.listen(3000);

